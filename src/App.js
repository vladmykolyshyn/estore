import { Routes, Route } from "react-router-dom";
import "./App.css";
import CatNav from "./Components/CatNav/CatNav";
import TopNav from "./Components/TopNav/TopNav";
import ProductDetails from "./Components/ProductDetails/ProductDetails";
import Cart from "./Components/Cart/Cart";
import MainComponent from "./Components/MainComponent/MainComponent";
import NotFound from "./Components/NotFound/NotFound";
import Wish from "./Components/WishList/WishList";

function App() {
  return (
    <div className="App">
      <TopNav />
      <CatNav />
      <Routes>
        <Route path="/" Component={MainComponent} />
        <Route path="/productDetails" Component={ProductDetails} />
        <Route path='/wishlist' Component={Wish}/>
        <Route path="/cart" Component={Cart} />
        <Route path="*" Component={NotFound} />
      </Routes>
    </div>
  );
}

export default App;
