import { useSelector } from "react-redux";
import "./_cart.scss";
import EmptyCart from "./EmptyCart/EmptyCart";
import FilledCart from "./FilledCart/FilledCart";

const Cart = () => {
  const cart = useSelector((state) => state.cartReducer);
  return (
    <div>
      {cart.cartItems.length === 0 ? <EmptyCart /> : <FilledCart />}
    </div>
  );
};

export default Cart;
