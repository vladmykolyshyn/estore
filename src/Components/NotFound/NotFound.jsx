import './_not-found.scss'

const NotFound = () => {
    return (
        <div className='error-container'>
            <h1>404</h1>
            <p>Page not found</p>
        </div>
    )
}

export default NotFound;