import "./_cat-nav.scss";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { Link } from "react-router-dom";
import { getCategories } from "../../Redux/Category/categoryAction";

const CatNav = () => {
  const dispatch = useDispatch();
  const categories = useSelector(state => state.categoryReducer.categories);
  
  useEffect(() => {
    dispatch(getCategories())
  }, [])
  
  return (
    <div>
      <div className="cat-nav-container container">
        <ul>
          <li className="list-items">
            <Link to="/">Home</Link>
          </li>
          {categories.map((category, index) => {
            if (category.parent_category_id === null) {
              return (
                <li key={index} className="list-items">
                  <a href="#">{category.category}</a>
                </li>
              );
            }
          })}
        </ul>
      </div>
    </div>
  );
};

export default CatNav;
