import { useSelector } from "react-redux";
import EmptyWish from "./EmptyWish/EmptyWish";
import FilledWish from "./FilledWish/FilledWish";

const Wish = () => {
    const wish = useSelector(state => state.wishReducer)

    return (
        <div>
            {wish.wishItems.length===0? <EmptyWish /> : <FilledWish />}
        </div>
    )
}

export default Wish;