import './_empty-wish.scss';
import { Link } from 'react-router-dom';

const EmptyWish = () => {
    return (
      <div className="p-4 ew-main-div">
        <span className="my-5 ew-text">The Wish List is Empty</span>
        <hr />
        <Link to="/">
          <div className="btn btn-warning my-3">
            <span>Continue Shopping</span>
          </div>
        </Link>
      </div>
    );
}

export default EmptyWish;