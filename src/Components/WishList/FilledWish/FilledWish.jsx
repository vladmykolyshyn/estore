import { useDispatch, useSelector } from 'react-redux';
import './_filled-wish.scss';
import { deleteWishItem } from '../../../Redux/Wish/wishSlice';
import { addCartItem } from '../../../Redux/Cart/cartSlice';

const FilledWish = () => {
    const wish = useSelector(state => state.wishReducer);
    const dispatch = useDispatch();

    const addToCart = (itemData) => {
      const payload = { ...itemData, quantity: 1 };
      dispatch(addCartItem(payload));
    };

    const deleteHandler = (item) => {
        dispatch(deleteWishItem(item))
    };

    return (
      <div>
        <div className="row my-5 fw-main-div">
          <div className="col-8 p-4">
            {wish.wishItems.map((item, key) => {
              return (
                <div key={key}>
                  <div className="row wish-item-card">
                    <div className="col-4">
                      <img
                        src={require("../../../assets/images/shop/" +
                          item.product_img)}
                        alt="item"
                      />
                    </div>
                    <div className="col-8">
                      <div className="p-3 wish-item-details">
                        <span className="wish-item-name">
                          {item.product_name}
                        </span>
                        <div className="wish-item-price">
                          <span>${item.price}</span>
                        </div>
                        <div>
                          <i className="fa fa-star text-warning" />
                          <i className="fa fa-star text-warning" />
                          <i className="fa fa-star text-warning" />
                          <i className="fa fa-star text-warning" />
                          <i className="fa fa-star text-warning" />
                        </div>
                        <hr />
                        <div className="cart-edit-container">
                          <div
                            className="btn btn-warning mx-4"
                            onClick={() => addToCart(item)}
                          >
                            <span>
                              <i className="fa fa-shopping-cart mx-2" />
                              To Cart
                            </span>
                          </div>
                          <div
                            className="btn btn-outline-danger mx-4"
                            onClick={() => deleteHandler(item)}
                          >
                            <span>
                              <i className="fa fa-trash mx-2" />
                              Remove Item
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr />
                </div>
              );
            })}
          </div>
          
        </div>
      </div>
    );
}

export default FilledWish;