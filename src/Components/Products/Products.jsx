import "./_products.scss";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getProducts } from "../../Redux/Product/productAction";
import { addCartItem } from "../../Redux/Cart/cartSlice";
import { Link } from "react-router-dom";
import { addWishItem } from "../../Redux/Wish/wishSlice";

const Products = () => {
  const dispatch = useDispatch();
  const products = useSelector(state => state.productReducer.products);
  
  useEffect(() => {
    dispatch(getProducts())
  }, [])

  const addToCart = (itemData) => {
    const payload = {...itemData, quantity:1}
    dispatch(addCartItem(payload));
  }

  const addToWish = (wishData) => {
    dispatch(addWishItem(wishData))
  }
  
  return (
    <div className="products-container">
      {
        products.map((product, index) => {
        return (
          <div className="mx-5 p-3 product-card" key={index}>
            <Link to="/productDetails" state={product}>
              <div className="product-image-container">
                <img
                  src={require("../../assets/images/shop/" +
                    product.product_img)}
                  alt="product"
                />
              </div>
            </Link>
            <div className="product-info">
              <h5>
                <Link to="/productDetails" state={product}>
                  {product.product_name}
                </Link>
              </h5>
              <p className="product-price">${product.price}</p>
            </div>
            <div className="product-rating">
              <i className="fa fa-star" />
              <i className="fa fa-star" />
              <i className="fa fa-star" />
              <i className="fa fa-star" />
              <i className="fa fa-star" />
            </div>
            <div
              className="my-3 cart-button-container"
              onClick={() => addToCart(product)}
            >
              <div className="cart-button">
                <div className="cart-icon-container">
                  <i className="fa fa-shopping-cart mx-4" />
                </div>
                <div className="cart-text-container mx-3">
                  <p>Add to cart</p>
                </div>
              </div>
            </div>
            <div
              className="mb-3 wish-button-container"
              onClick={() => addToWish(product)}
            >
              <div className="cart-button">
                <div className="cart-icon-container">
                  <i className="fa fa-heart mx-4" />
                </div>
                <div className="cart-text-container mx-3">
                  <p>Add to wishlist</p>
                </div>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default Products;
