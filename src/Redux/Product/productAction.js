import { createAsyncThunk } from "@reduxjs/toolkit";

export const getProducts = createAsyncThunk(
    'getProducts',
    () => {
        const products = fetch("http://localhost:3001/products")
        .then((res) => res.json());
        return products;
    }
);