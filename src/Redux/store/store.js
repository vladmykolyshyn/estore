import { configureStore } from "@reduxjs/toolkit";
import categorySlice from "../Category/categorySlice";
import productSlice from "../Product/productSlice"
import cartSlice from "../Cart/cartSlice";
import wishSlice from "../Wish/wishSlice";

export const store = configureStore({
    reducer: {
        categoryReducer: categorySlice,
        productReducer: productSlice,
        cartReducer: cartSlice,
        wishReducer: wishSlice,
    }
})