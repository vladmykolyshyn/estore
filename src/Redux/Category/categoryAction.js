import { createAsyncThunk } from "@reduxjs/toolkit";

export const getCategories = createAsyncThunk(
    'getCategories',
    () => {
        const categories = fetch("http://localhost:3001/categories")
        .then((res) => res.json())
        return categories;
    }
)