import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    wishItems: [],
    totalItems: 0,

};

const wishSlice = createSlice({
    name: 'wish',
    initialState,
    reducers: {
        addWishItem: (state, action) => {
            let item_exists = state.wishItems.find((item) => item.id === action.payload.id);
            if (!item_exists) {
                state.wishItems = [...state.wishItems, action.payload];
                state.totalItems = ++state.totalItems;
            }
        },
        deleteWishItem: (state, action) => {
            let filteredWish = state.wishItems.filter((elem) => {
                return elem.id!==action.payload.id
            });
            state.wishItems = filteredWish;
            state.totalItems = --state.totalItems;
        }
        
    }
})

export const { addWishItem, deleteWishItem } = wishSlice.actions;
export default wishSlice.reducer;