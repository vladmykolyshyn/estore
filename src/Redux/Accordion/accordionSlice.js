import { createSlice } from "@reduxjs/toolkit";

const initialState = [
  {
    category: "Men",
    items: ["Coats", "Jackets", "Party Wear", "Shirts"],
  },
  {
    category: "Women",
    items: ["Coats", "Jackets", "Party Wear", "Shirts"],
  },
  {
    category: "Kids",
    items: ["Coats", "Jackets", "Party Wear", "Shirts"],
  },
];

export const accordionSlice = createSlice({
    name: "accordionSlice",
    initialState,
    reducers:{}
});
